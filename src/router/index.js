import { createRouter, createWebHistory } from 'vue-router'
import EditWorkaday from '@/components/EditWorkaday.vue'
import WorkadayForm from '@/components/WorkadayForm.vue'
import ListWorkadays from '@/components/ListWorkadays.vue'
import ListUsers from '@/components/ListUsers.vue'
import Login from '@/components/Login.vue'
import Home from "@/components/Home.vue";
import Register from "@/components/Register.vue";

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: { requiresAuth: true },
    children: [
      {
        path: '/list-users',
        name: 'list-users',
        component: ListUsers,
        meta: { requiresAuth: true }
      },
      {
        path: '/create-workaday',
        name: 'create-workaday',
        component: WorkadayForm,
        meta: { requiresAuth: true }
      },
      {
        path: '/list-workadays',
        name: 'list-workadays',
        component: ListWorkadays,
        meta: { requiresAuth: true }
      },
      {
        path: '/edit-workaday/:workadayData',
        name: 'EditWorkaday',
        component: EditWorkaday,
        meta: { requiresAuth: true }
      },
  ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/logout',
    redirect: '/login'
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !isAuthenticated()) {
    next({ name: 'login' });
  } else {
    next();
  }
});

// Função para verificar se o usuário está autenticado
function isAuthenticated() {
  // Verifica se há um token de autenticação no localStorage
  const token = localStorage.getItem('token');
  return !!token; // Retorna true se o token existir, false se não existir
}


export default router
